import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from './pages/Login'
import Dashboard from './pages/Dashboard'
import Registro from './pages/Registro'

Vue.use(VueRouter)
const routes = [
    {
        path:'/login',
        component:Login
    },
    {
        path:'/dashboard',
        component: Dashboard
    },
    {
        path: '/registro',
        component : Registro
    }
]
const router = new VueRouter({
    routes // short for `routes: routes`
  })
export default router