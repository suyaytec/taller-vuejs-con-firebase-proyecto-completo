import firebase from 'firebase'
var config = {
    apiKey: "#############",
    authDomain: "#############",
    databaseURL: "#############",
    projectId: "#############",
    storageBucket: "#############",
    messagingSenderId: "#############"
  };
firebase.initializeApp(config);
const firestore = firebase.firestore();
const settings = {timestampsInSnapshots: true};
firestore.settings(settings);

export default firebase