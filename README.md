## Taller VueJS con Firebase - Chat y Gestor de tareas en vivo

El proyecto completo para el Taller VueJS con Firebase dado por [SUYAY Technology](https://www.facebook.com/suyaytec/) para la Universidad Nacional Mayor de San Marcos. 
**Tecnologías y librerías usadas:**
 1. [VueJS](https://vuejs.org/) 
 2. [Vuetify](https://vuetifyjs.com/es-MX/) - Para usar componentes basados en Material Design.
 3. [Vue Router](https://router.vuejs.org/guide/) - Para manejar la vista de componentes basada en la URL. (/login, /registro y /dashboard)
 4. [Firebase](https://firebase.google.com/) - Plataforma que facilita el desarrollo de aplicaciones web y mobiles. Nos proporciona servicios de base de datos, administración de usuario, almacenamiento de archivos, etc...

# Instalación

 1. Instalar [NodeJS](https://nodejs.org/es/download/) para poder utilizar el comando "npm".
 2. Descargar el repositorio y colocarlo dentro de una carpeta.
 3. Ingresar a la web de [Firebase](https://firebase.google.com/) y crear un nuevo proyecto. Saltate este paso si ya lo tienes creado.
 4. Activar la base de datos (modo de prueba), storage y authentication (correo electrónico y contraseña, Facebook y Google).
 5.  Modificar el archivo **firebase.js** con las credenciales propias de su proyecto.
 6. Abrir la linea de comando e ir a la carpeta del proyecto.
 7. Ingresar el comando **npm install**
 8. Ingresar el comando **npm run dev** 